'use strict';

const Joi = require('joi');
const ObjectAssign = require('object-assign');
const BaseModel = require('hapi-mongo-models').BaseModel;
const StatusEntry = require('./status-entry');
const NoteEntry = require('./note-entry');


const ATM = BaseModel.extend({
    constructor: function (attrs) {

        ObjectAssign(this, attrs);
    }
});


ATM._collection = 'atm';


ATM.schema = Joi.object().keys({
    _id: Joi.object(),
	atm : Joi.object().keys({
		type        : Joi.string().required(),
		direction   : Joi.any().required(),
		supported   : Joi.array().min(1).required(),
		fees        : Joi.string().optional(),
		limits      : Joi.string().optional(),
		details     : Joi.string().optional()
	}),
	location : Joi.object().keys({
		lat         : Joi.number().precision(8).required(),
		long        : Joi.number().precision(8).required(),
		location    : Joi.string().required(),
		address     : Joi.object().keys({
			street      : Joi.string().required(),
			city        : Joi.string().required(),
			state       : Joi.string().optional(),
			zip         : Joi.string().optional(),
			country     : Joi.string().required()
		}),
		phone       : Joi.string().optional(),
		openHours   : Joi.string().optional()
	}),
	operator : Joi.object().keys({
		id          : Joi.string().required()   
	}),
    timeCreated: Joi.date()
});


ATM.indexes = [
    { key: { 'user.id': 1 } },
    { key: { 'user.name': 1 } }
];


ATM.create = function (payload, callback) {

	let document = payload;
	document.timeCreated = new Date();

    this.insertOne(document, (err, docs) => {

        if (err) {
            return callback(err);
        }

        callback(null, docs[0]);
    });

};


ATM.findByUsername = function (username, callback) {

    const query = { 'user.name': username.toLowerCase() };
    this.findOne(query, callback);
};


module.exports = ATM;

'use strict';

const Boom = require('boom');
const Async = require('async');
const Joi = require('joi');
const AuthPlugin = require('../auth');


const internals = {};


internals.applyRoutes = function (server, next) {

    const ATM 		= server.plugins['hapi-mongo-models'].ATM;
    const Account 	= server.plugins['hapi-mongo-models'].Account;
    const User 		= server.plugins['hapi-mongo-models'].User;


    server.route({
        method: 'GET',
        path: '/atm',
        config: {
            validate: {
                query: {
					data: Joi.any(),
                    fields: Joi.string(),
                    sort: Joi.string().default('_id'),
                    limit: Joi.number(),
                    page: Joi.number().default(1)
                }
            }
        },
        handler: function (request, reply) {

            const query = request.query.data ? getQueryParams(request.query.data) : {};
            const fields = request.query.fields;
            const sort = request.query.sort;
            const limit = request.query.limit;
            const page = request.query.page;

            ATM.pagedFind(query, fields, sort, limit, page, (err, results) => {

                if (err) {
                    return reply(err);
                }

                reply(results);
            });
        }
    });


    server.route({
        method: 'GET',
        path: '/atm/{id}',
        config: {},
        handler: function (request, reply) {

            ATM.findById(request.params.id, (err, atm) => {

                if (err) {
                    return reply(err);
                }

                if (!atm) {
                    return reply(Boom.notFound('Document not found.'));
                }

                reply(atm);
            });
        }
    });

    server.route({
        method: 'GET',
        path: '/atm/{id}/operator',
        config: {},
        handler: function (request, reply) {

            ATM.findById(request.params.id, (err, atm) => {

                if (err) {
                    return reply(err);
                }

                if (!atm) {
                    return reply(Boom.notFound('Document not found.'));
                }

				if(atm.operator.id) {

					Account.findById(atm.operator.id, (err, operator) => {
						if (err) {
							reply(atm);
						}

						if (!operator) {
							reply(atm);
						}
	
						if (operator) {
							atm.operator.name 	= operator.company;
							atm.operator.phone 	= operator.details.phone;
							atm.operator.email 	= operator.details.email;
							
							reply(atm);
						}
					})

				}

				if(!atm.operator.id) {
					reply(atm);
				}

            });
        }
    });



    server.route({
        method: 'POST',
        path: '/atm',
        config: {
            auth: {
                strategy: 'simple',
                scope: ['account', 'admin']
            },
            validate: {
                payload: {               
                    atm : Joi.object().keys({
                        type        : Joi.string().required(),
                        direction   : Joi.any().required(),
                        supported   : Joi.array().min(1).required(),
                        fees        : Joi.any().optional(),
                        limits      : Joi.any().optional(),
                        details     : Joi.any().optional()
                    }),
                    location : Joi.object().keys({
                        lat         : Joi.number().precision(8).required(),
                        long        : Joi.number().precision(8).required(),
                        location    : Joi.string().required(),
                        address     : Joi.object().keys({
                            street      : Joi.string().required(),
                            city        : Joi.string().required(),
                            state       : Joi.any().optional(),
                            zip         : Joi.any().optional(),
                            country     : Joi.string().required()
                        }),
                        phone       : Joi.any().optional(),
                        openHours   : Joi.any().optional()
                    }),
                    operator : Joi.object().keys({
                        id          : Joi.string().required()   
                    })
                }
            }
        },
        handler: function (request, reply) {

            const payload = request.payload;

            ATM.create(payload, (err, atm) => {

                if (err) {
                    return reply(err);
                }

                reply(atm);
            });
        }
    });


    server.route({
        method: 'PUT',
        path: '/atm/{id}',
        config: {
            auth: {
                strategy: 'simple',
                scope: [ 'account', 'admin' ]
            },
            validate: {
                payload: {               
                    atm : Joi.object().keys({
                        type        : Joi.string().required(),
                        direction   : Joi.any().required(),
                        supported   : Joi.array().min(1).required(),
                        fees        : Joi.any().optional(),
                        limits      : Joi.any().optional(),
                        details     : Joi.any().optional()
                    }),
                    location : Joi.object().keys({
                        lat         : Joi.number().precision(8).required(),
                        long        : Joi.number().precision(8).required(),
                        location    : Joi.string().required(),
                        address     : Joi.object().keys({
                            street      : Joi.string().required(),
                            city        : Joi.string().required(),
                            state       : Joi.any().optional(),
                            zip         : Joi.any().optional(),
                            country     : Joi.string().required()
                        }),
                        phone       : Joi.any().optional(),
                        openHours   : Joi.any().optional()
                    }),
                    operator : Joi.object().keys({
                        id          : Joi.any().optional()   
                    })
                }
            }
        },
        handler: function (request, reply) {

            const id 		= request.params.id;
			const payload 	= request.payload;

            const update = {
                $set: {
                    atm : {
                        type        : payload.atm.type,
                        supported   : payload.atm.supported,
                        direction   : payload.atm.direction,
                        fees        : payload.atm.fees,
                        limits      : payload.atm.limits,
                        details     : payload.atm.details
                    },
                    location : {
                        lat         : payload.location.lat,
                        long        : payload.location.long,
                        location    : payload.location.location,
                        address     : {
                            street      : payload.location.address.street,
                            city        : payload.location.address.city,
                            state       : payload.location.address.state,
                            zip         : payload.location.address.zip,
                            country     : payload.location.address.country
                        },
                        phone       : payload.location.phone,
                        openHours   : payload.location.openHours
                    },
                    "operator.id" 	: payload.operator.id || null 
                }
            };

            ATM.findByIdAndUpdate(id, update, (err, atm) => {

                if (err) {
                    return reply(err);
                }

                if (!atm) {
                    return reply(Boom.notFound('Document not found.'));
                }

                reply(atm);
            });
        }
    });

    server.route({
        method: 'DELETE',
        path: '/atm/{id}',
        config: {
            auth: {
                strategy: 'simple',
                scope: [ 'account', 'admin' ]
            },
            pre: [
                AuthPlugin.preware.ensureAdminGroup('root')
            ]
        },
        handler: function (request, reply) {

            ATM.findByIdAndDelete(request.params.id, (err, atm) => {

                if (err) {
                    return reply(err);
                }

                if (!atm) {
                    return reply(Boom.notFound('Document not found.'));
                }

                reply({ message: 'Success.' });
            });
        }
    });


    next();
};


exports.register = function (server, options, next) {

    server.dependency(['auth', 'hapi-mongo-models'], internals.applyRoutes);

    next();
};


exports.register.attributes = {
    name: 'atm'
};

const getQueryParams = data => {

	let queryData = {}

	if (Array.isArray(data)){
		data.map(query => {
			let string = query.split(':')
				queryData[string[0]] = string[1] || null
		})
	}else{
		let string = data.split(':')
		queryData[string[0]] = string[1] || null
	}
	
	return queryData
}
